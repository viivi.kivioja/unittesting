const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {

    let myvar = undefined;
    before(() => {
        console.log('sum 1+1 is: ' + mylib.sum(1,1));
    })
    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1);
        expect(result).to.equal(2);
    })
    it('Result return 5 when using minus function with a=10, b=5', () => {
        const result = mylib.minus(10,5);
        assert(result == 5);
    })
    it.skip('Myvar should exist', () => {
        should.exist(myvar);
    })
    after(() => {
        console.log('substraction 10-5 is: ' + mylib.minus(10,5));
    })
})